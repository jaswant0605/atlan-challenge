import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:path/path.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:sqflite/sqflite.dart';

import 'bloc/form_bloc_bloc.dart';
import 'bloc/form_bloc_event.dart';
import 'bloc/form_bloc_state.dart';
import 'constants/AppConstants.dart';
import 'models/FeedbackResponse.dart';
import 'models/ServerResponse.dart';
import 'thankYouScreen.dart' as TSC;

class FeedbackForm extends StatefulWidget {
  final ServerResponse serverResponse;

  FeedbackForm(this.serverResponse);

  @override
  _FeedbackFormState createState() => _FeedbackFormState();
}

class _FeedbackFormState extends State<FeedbackForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> _selectedValues = [];

  final _formBloc = FormBlocBloc();

  @override
  void initState() {
    for (int i = 0; i < widget.serverResponse.fields.length; i++) {
      _selectedValues.add("");
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text(
              "FEEDBACK FORM",
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 24, letterSpacing: 2),
            ),
          ),
          body: BlocProvider(
            builder: (BuildContext context) => _formBloc,
            child: BlocListener(
              listener: (BuildContext context, FormBlocState state) {
                if (state is OnDataSubmitted) {
                  Navigator.of(context).pushReplacement(CupertinoPageRoute(
                      builder: (context) =>
                          TSC.ThankYouScreen(widget.serverResponse)));
                }
              },
              bloc: _formBloc,
              child: BlocBuilder(
                  bloc: _formBloc,
                  builder: (BuildContext context, FormBlocState state) {
                    if (state is OnDataSubmitting) {
                      return Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 5,
                        ),
                      );
                    } else {
                      return _formsWidgetTree();
                    }
                  }),
            ),
          )),
    );
  }

  Widget _formsWidgetTree() {
    return SafeArea(
        child: Stack(
      children: <Widget>[
        SingleChildScrollView(
            scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                ListView.builder(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemCount: widget.serverResponse.fields.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return _generateDesiredWidget(index);
                  },
                ),
                SizedBox(height: 48)
              ],
            )),
        Align(
          alignment: Alignment.bottomCenter,
          child: GestureDetector(
            onTap: _submitFeedbackClicked,
            child: Container(
              color: Colors.black,
              height: 48,
              width: MediaQuery.of(_scaffoldKey.currentContext).size.width,
              child: Center(
                child: Text(
                  "SUBMIT FEEDBACK",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ), //Width of screen
            ),
          ),
        ),
      ],
    ));
  }

  Widget _generateDesiredWidget(int position) {
    switch (widget.serverResponse.fields[position].type) {
      case AppConstants.SHORT_TEXT:
        return _textField(position, TextInputType.text);
      case AppConstants.DROPDOWN:
        return _dropdownWidget(position);
      case AppConstants.NUMBER:
        return _textField(position, TextInputType.number);
      case AppConstants.EMAIL:
        return _textField(position, TextInputType.emailAddress);
      case AppConstants.PHONE_NO:
        return _textField(position, TextInputType.phone);
      case AppConstants.RATING:
        return _ratingWidget(position);
        break;
      case AppConstants.DATE:
        return _datePicker(position);
        break;
      case AppConstants.YES_NO:
        if (_selectedValues[position] == "") {
          _selectedValues[position] = "false";
        }
        return _yesNoWidget(position);
        break;
      default:
        return Container();
        break;
    }
  }

  Widget _textField(int index, TextInputType textInputType) {
    return Container(
      margin: EdgeInsets.only(left: 14.0, right: 14.0, top: 12.0),
      decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.all(Radius.circular(10))),
      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
      child: TextFormField(
        keyboardType: textInputType,
        onChanged: (string) {
          _selectedValues[index] = string;
        },
        decoration: InputDecoration(
          hintText: widget.serverResponse.fields[index].title,
          hintStyle: new TextStyle(color: Colors.grey[600], fontSize: 16.0),
          border: InputBorder.none,
        ),
        style: new TextStyle(color: Colors.black, fontSize: 16.0),
      ),
    );
  }

  Widget _ratingWidget(int index) {
    return Container(
        margin: EdgeInsets.only(left: 14.0, right: 14.0, top: 12.0),
        decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10))),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20.0),
            Text(
              widget.serverResponse.fields[index].title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
            BlocProvider(
              builder: (BuildContext context) => _formBloc,
              child: BlocBuilder(
                  bloc: _formBloc,
                  builder: (BuildContext context, FormBlocState state) {
                    if (state is OnRatingChanged) {
                      return SmoothStarRating(
                          allowHalfRating: false,
                          onRatingChanged: (v) {
                            _selectedValues[state.index] = "$v";
                            print(_selectedValues);
                            _formBloc.dispatch(RatingChanged("$v", index));
                          },
                          starCount: widget.serverResponse.fields[index]
                              .fieldProperties.steps,
                          rating: _selectedValues[index] != ""
                              ? double.tryParse(_selectedValues[index])
                              : 0,
                          size: 40.0,
                          color: Colors.black,
                          borderColor: Colors.grey,
                          spacing: 0.0);
                    } else {
                      return SmoothStarRating(
                          allowHalfRating: false,
                          onRatingChanged: (v) {
                            _selectedValues[index] = "$v";
                            _formBloc.dispatch(RatingChanged("$v", index));
                          },
                          starCount: widget.serverResponse.fields[index]
                              .fieldProperties.steps,
                          rating: _selectedValues[index] != ""
                              ? double.tryParse(_selectedValues[index])
                              : 0,
                          size: 40.0,
                          color: Colors.black,
                          borderColor: Colors.grey,
                          spacing: 0.0);
                    }
                  }),
            ),
          ],
        ));
  }

  Widget _yesNoWidget(int index) {
    return Container(
        margin: EdgeInsets.only(left: 14.0, right: 14.0, top: 12.0),
        decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10))),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20.0),
            Text(
              widget.serverResponse.fields[index].title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
            BlocProvider(
              builder: (BuildContext context) => _formBloc,
              child: BlocBuilder(
                bloc: _formBloc,
                builder: (BuildContext context, FormBlocState state) {
                  return Checkbox(
                    value: _selectedValues[index] == "true" ? true : false,
                    activeColor: Colors.black,
                    onChanged: (bool) {
                      _selectedValues[index] = "$bool";
                      _formBloc.dispatch(CheckboxChanged(bool, index));
                    },
                  );
                },
              ),
            ),
          ],
        ));
  }

  Widget _dropdownWidget(int position) {
    List<String> options = [];
    for (int i = 0;
        i <
            widget
                .serverResponse.fields[position].fieldProperties.choices.length;
        i++) {
      options.add(widget
          .serverResponse.fields[position].fieldProperties.choices[i].label);
    }
    return Container(
        margin: EdgeInsets.only(left: 14.0, right: 14.0, top: 12.0),
        decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.all(Radius.circular(10))),
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
        child: BlocProvider(
            builder: (BuildContext context) => _formBloc,
            child: BlocBuilder(
                bloc: _formBloc,
                builder: (BuildContext context, FormBlocState state) {
                  if (state is OnDropDownValueChanged) {
                    return DropdownButton<String>(
                      value: _selectedValues[position],
                      underline: Container(
                        height: 1,
                      ),
                      style: TextStyle(fontSize: 16, color: Colors.black),
                      hint: Text(
                        widget.serverResponse.fields[position].title,
                        style: TextStyle(color: Colors.black),
                      ),
                      iconEnabledColor: Colors.black,
                      items: options.map((value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        _selectedValues[position] = value;
                        _formBloc
                            .dispatch(DropdownValueSelected(value, position));
                      },
                    );
                  } else {
                    return DropdownButton<String>(
                      value: options.contains(_selectedValues[position])
                          ? _selectedValues[position]
                          : null,
                      underline: Container(
                        height: 1,
                      ),
                      style: TextStyle(fontSize: 16, color: Colors.black),
                      hint: Text(
                        widget.serverResponse.fields[position].title,
                        style: TextStyle(color: Colors.black),
                      ),
                      iconEnabledColor: Colors.black,
                      items: options.map((value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        _selectedValues[position] = value;
                        _formBloc
                            .dispatch(DropdownValueSelected(value, position));
                      },
                    );
                  }
                })));
  }

  Widget _datePicker(int position) {
    return GestureDetector(
      onTap: () {
        _onDateClicked(position);
      },
      child: Container(
          margin: EdgeInsets.only(left: 14.0, right: 14.0, top: 12.0),
          decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10))),
          padding:
              EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
          child: BlocProvider(
              builder: (BuildContext context) => _formBloc,
              child: BlocBuilder(
                  bloc: _formBloc,
                  builder: (BuildContext context, FormBlocState state) {
                    if (state is OnDateChanged) {
                      _selectedValues[position] = state.newDate;
                      return TextField(
                        enabled: false,
                        decoration: InputDecoration(
                          labelText: _selectedValues[position],
                          hintText:
                              widget.serverResponse.fields[position].title,
                          hintStyle:
                              new TextStyle(color: Colors.grey[600], fontSize: 16.0),
                          labelStyle:
                              new TextStyle(color: Colors.black, fontSize: 16.0),
                          border: InputBorder.none,
                        ),
                        style:
                            new TextStyle(color: Colors.black, fontSize: 16.0),
                      );
                    } else {
                      return TextField(
                        enabled: false,
                        decoration: InputDecoration(
                          labelText: _selectedValues[position] != ""
                              ? _selectedValues[position]
                              : null,
                          hintText:
                              widget.serverResponse.fields[position].title,
                          hintStyle:
                              new TextStyle(color: Colors.grey[600], fontSize: 16.0),
                          labelStyle:
                              new TextStyle(color: Colors.black, fontSize: 16.0),
                          border: InputBorder.none,
                        ),
                        style:
                            new TextStyle(color: Colors.black, fontSize: 16.0),
                      );
                    }
                  }))),
    );
  }

  _onDateClicked(int position) {
    DatePicker.showDatePicker(_scaffoldKey.currentContext, showTitleActions: true,
        onConfirm: (DateTime date) {
      _formBloc.dispatch(DateChanged(
          date,
          position,
          widget.serverResponse.fields[position].fieldProperties.separator,
          widget.serverResponse.fields[position].fieldProperties.structure));
    });
  }

  _submitFeedbackClicked() async {
    if (_areFieldsValid()) {
      _formBloc
          .dispatch(OnSubmitClicked(widget.serverResponse, _selectedValues));
     var result = await prepareData();
     if (result) {
       Navigator.of(_scaffoldKey.currentContext).pushReplacement(CupertinoPageRoute(
           builder: (context) =>
               TSC.ThankYouScreen(widget.serverResponse)));
     }
    }
  }

  @override
  void dispose() {
    _formBloc.dispose();
    super.dispose();
  }

  bool _areFieldsValid() {
    print(_selectedValues);
    for (int i = 0; i < widget.serverResponse.fields.length; i++) {
      if (widget.serverResponse.fields[i].validations.required &&
          _selectedValues[i] == "") {
        _showSnackbar("Please ${widget.serverResponse.fields[i].title}");
        return false;
      }

      if (widget.serverResponse.fields[i].type == AppConstants.EMAIL && widget.serverResponse.fields[i].validations.required) {
        if(!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_selectedValues[i])) {
          _showSnackbar("Please Enter a Valid Email Address");
          return false;
        }
      }

      if (widget.serverResponse.fields[i].title.toLowerCase().contains("age") &&
          widget.serverResponse.fields[i].validations.required) {
        if (int.tryParse(_selectedValues[i]) < 1 ||
            int.tryParse(_selectedValues[i]) > 90) {
          _showSnackbar("Please enter a valid age");
          return false;
        }
      }
    }

    return true;
  }

  _showSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: new Text(
        message,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      backgroundColor: Colors.blue,
    ));
  }


  Future<bool> prepareData() async {
    final Database database = await openDatabase(
      //Setting path to database
        join(await getDatabasesPath(), 'feedback_database.db'),
        onCreate: (db, version) {
          return db.execute(
              "CREATE TABLE feedbacks (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, timeStamp TEXT, field TEXT, value TEXT)");
        }, version: 1);
    var db = database;
    if (db != null) {
      String timestamp = "${DateTime.now().millisecondsSinceEpoch}";
      for (int i = 0; i < _selectedValues.length; i++) {
        FeedbackResponse feedbackResponse = FeedbackResponse(
            field: widget.serverResponse.fields[i].title,
            value: _selectedValues[i],
            timeStamp: timestamp);
        bool result = insertFeedback(feedbackResponse, db);
        if (result) {
          if (i == _selectedValues.length - 1) {

            return true;
          }
        }
      }
    }
  }

  Future<bool> _startTimer(int secondsToLoad) {
    return Future.delayed(
        Duration(seconds: secondsToLoad),
            () {
          return true;
        });
  }

  bool insertFeedback(
      FeedbackResponse feedbackResponse, Database database) {
    // Get a reference to the database.
    final Database db = database;

    // Insert the Feedback into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    db.insert(
      AppConstants.FEEDBACKS_DATABASE_KEY,
      feedbackResponse.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    return true;
  }
}
