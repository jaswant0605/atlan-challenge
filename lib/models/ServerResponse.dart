class ServerResponse {
  final String id;
  final String title;
  final Settings settings;
  final List<WelcomeScreen> welcomeScreens;
  final List<ThankYouScreen> thankYouScreens;
  final List<Field> fields;

  ServerResponse({this.id, this.title, this.settings, this.welcomeScreens, this.thankYouScreens, this.fields});
  factory ServerResponse.fromJson(Map json) {
    return ServerResponse(
      id: json["id"],
      title: json["title"],
      settings: Settings.fromJson(json["settings"]),
      welcomeScreens: (json["welcome_screens"] as List).map((i) => WelcomeScreen.fromJson(i)).toList(),
      thankYouScreens: (json["thankyou_screens"] as List).map((i) => ThankYouScreen.fromJson(i)).toList(),
      fields: (json["fields"] as List).map((i) => Field.fromJson(i)).toList(),
    );
  }
}

class Settings {
  final String language;
  final String progressBar;
  final bool showProgressBar;

  Settings({this.language, this.progressBar, this.showProgressBar});
  factory Settings.fromJson(Map json) {
    return Settings(
      language: json["language"],
      progressBar: json["progress_bar"],
      showProgressBar: json["show_progress_bar"]
    );
  }
}

class WelcomeScreen {
  final String title;
  final Properties properties;

  WelcomeScreen({this.title, this.properties});
  factory WelcomeScreen.fromJson(Map json) {
    return WelcomeScreen(
      title: json["title"],
      properties: Properties.fromJson(json["properties"])
    );
  }
}

class ThankYouScreen {
  final String title;
  final Properties properties;

  ThankYouScreen({this.title, this.properties});
  factory ThankYouScreen.fromJson(Map json) {
    return ThankYouScreen(
        title: json["title"],
        properties: Properties.fromJson(json["properties"])
    );
  }
}

class Properties {
  final bool showButton;
  final bool shareIcons;
  final String buttonMode;
  final String buttonText;
  final String description;


  Properties({this.shareIcons, this.buttonMode, this.buttonText, this.description, this.showButton});
  factory Properties.fromJson(Map json) {
    return Properties(
      shareIcons: json["share_icons"],
      showButton: json["show_button"],
      buttonMode: json["button_mode"],
      buttonText: json["button_text"],
      description: json["description"]
    );
  }

}


class Field {
  final String id;
  final String title;
  final String type;
  final Validations validations;
  final FieldProperties fieldProperties;

  Field({this.id, this.title, this.validations, this.type, this.fieldProperties});
  factory Field.fromJson(Map json) {
    return Field(
      id: json["id"],
      title: json["title"],
      type: json["type"],
      validations: Validations.fromJson(json["validations"]),
      fieldProperties: json["properties"] != null ? FieldProperties.fromJson(json["properties"]) : null
    );
  }
}

class FieldProperties {
  final bool alphabeticalOrder;
  final List<Choice> choices;
  final String structure;
  final String separator;
  final int steps;

  FieldProperties({this.alphabeticalOrder, this.choices, this.separator, this.structure, this.steps});
  factory FieldProperties.fromJson(Map json) {
    return FieldProperties(
      alphabeticalOrder: json["alphabetical_order"],
      choices: json["choices"] != null ? (json["choices"] as List).map((i) => Choice.fromJson(i)).toList() : null,
      separator: json["separator"],
      structure: json["structure"],
      steps: json["steps"]
    );
  }
}

class Choice {
  final String label;
  Choice({this.label});
  factory Choice.fromJson(Map json) {
    return Choice(
      label: json["label"]
    );
  }
}

class Validations {
  final bool required;

  Validations({this.required});
  factory Validations.fromJson(Map json) {
    return Validations(
      required: json["required"]
    );
  }
}