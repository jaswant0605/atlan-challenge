class FeedbackResponse {
  final int id;
  final String timeStamp;
  final String field;
  final String value;

  FeedbackResponse({this.id, this.field, this.value, this.timeStamp});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'timeStamp': timeStamp,
      'field': field,
      'value': value
    };
  }
}