import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class FormBlocState extends Equatable {
  FormBlocState([List props = const []]) : super(props);
}

class InitialFormBlocState extends FormBlocState {

}

class OnRatingChanged extends FormBlocState {
  final String newRating;
  final int index;

  OnRatingChanged(this.newRating, this.index) : super([newRating, index]);
}

class OnDropDownValueChanged extends FormBlocState {
  final String selectedItem;
  final int widgetPosition;

  OnDropDownValueChanged(this.selectedItem, this.widgetPosition) : super([selectedItem, widgetPosition]);
}

class OnCheckboxChanged extends FormBlocState {
  final bool newVal;
  final int position;

  OnCheckboxChanged(this.newVal, this.position) : super([newVal, position]);
}

class OnDateChanged extends FormBlocState {
  final String newDate;
  final int position;

  OnDateChanged(this.position, this.newDate) : super([position, newDate]);
}

class OnDataSubmitting extends FormBlocState {

}

class OnDataSubmitted extends FormBlocState {
  final bool result;
  OnDataSubmitted(this.result) : super([result]);
}

