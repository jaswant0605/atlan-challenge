import 'package:atlan_challenge/models/ServerResponse.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class FormBlocEvent extends Equatable {
  FormBlocEvent([List props = const []]) : super(props);
}

class RatingChanged extends FormBlocEvent {
  final String newRating;
  final int index;
  RatingChanged(this.newRating, this.index) : super([newRating]);
}

class DropdownValueSelected extends FormBlocEvent {
  final String selectedItem;
  final int widgetPosition;

  DropdownValueSelected(this.selectedItem, this.widgetPosition) : super([selectedItem, widgetPosition]);
}

class CheckboxChanged extends FormBlocEvent {
  final bool newVal;
  final int position;

  CheckboxChanged(this.newVal, this.position) : super([newVal, position]);
}

class DateChanged extends FormBlocEvent {
  final DateTime dateTime;
  final int position;
  final String structure;
  final String separator;

  DateChanged(this.dateTime, this.position, this.separator, this.structure) : super([dateTime, position, separator, structure]);
}

class OnSubmitClicked extends FormBlocEvent {
  final ServerResponse serverResponse;
  final List<String> submittedAnswers;

  OnSubmitClicked(this.serverResponse, this.submittedAnswers) : super([serverResponse, submittedAnswers]);
}

class OnDatabaseSubmitted extends FormBlocEvent {

}


