import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SplashEvent extends Equatable {
  SplashEvent([List props = const []]) : super(props);
}

//Event
class HandleNavigation extends SplashEvent {
  HandleNavigation() : super([]);
}
