import 'package:atlan_challenge/models/ServerResponse.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SplashState extends Equatable {
  SplashState([List props = const []]) : super(props);
}

class InitialSplashState extends SplashState {
}

class ShowLoading extends SplashState {
}

class SplashEnded extends SplashState {
  final ServerResponse serverResponse;
  SplashEnded(this.serverResponse) : super([serverResponse]);
}