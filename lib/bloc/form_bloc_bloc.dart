import 'dart:async';

import 'package:atlan_challenge/models/FeedbackResponse.dart';
import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import './bloc.dart';
import 'form_bloc_event.dart';

class FormBlocBloc extends Bloc<FormBlocEvent, FormBlocState> {
  @override
  FormBlocState get initialState => InitialFormBlocState();

  @override
  Stream<FormBlocState> mapEventToState(
    FormBlocEvent event,
  ) async* {
    if (event is RatingChanged) {
      yield OnRatingChanged(event.newRating, event.index);
    } else if (event is DropdownValueSelected) {
      yield OnDropDownValueChanged(event.selectedItem, event.widgetPosition);
    } else if (event is CheckboxChanged) {
      yield OnCheckboxChanged(event.newVal, event.position);
    } else if (event is DateChanged) {
      DateTime date = event.dateTime;
      DateTime newDate =
          new DateTime.fromMillisecondsSinceEpoch(date.millisecondsSinceEpoch);

      String dateFormat = event.structure;
      List<String> individualLetters = [];
      individualLetters.addAll(dateFormat.runes
          .map((rune) => new String.fromCharCode(rune))
          .toList());

      String currentLetter = dateFormat[0];
      int count = 1;
      for (int i = 0; i < dateFormat.length - 1; i++) {
        if (dateFormat[i + 1] != currentLetter) {
          currentLetter = dateFormat[i + 1];
          individualLetters.insert(i + count, event.separator);
          count++;
          i++;
        }
      }

      for (int i = 0; i < individualLetters.length; i++) {
        if (individualLetters[i] != "M") {
          individualLetters[i] = individualLetters[i].toLowerCase();
        }
      }

      var format = DateFormat(individualLetters.join());
      var dateString = format.format(newDate);

      yield OnDateChanged(event.position, dateString);
    } else if (event is OnSubmitClicked) {
      yield OnDataSubmitting();
    }

  }


}
