import 'dart:async';
import 'dart:convert';
import 'package:atlan_challenge/constants/AppConstants.dart';
import 'package:atlan_challenge/models/ServerResponse.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './bloc.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  @override
  SplashState get initialState => InitialSplashState();

  @override
  Stream<SplashState> mapEventToState(
    SplashEvent event,
  ) async* {
    if (event is HandleNavigation) {
      yield ShowLoading();
      String string = await getServerResponse();
      if (string != null) {
        ServerResponse serverResponse = ServerResponse.fromJson(json.decode(string));
        final savedToSharedPrefs = await saveToSharedPref(string);
        if (savedToSharedPrefs) {
          yield SplashEnded(serverResponse);
        }
      } else {
        String string = await readFromSharedPref();
        if (string != null) {
          ServerResponse serverResponse = ServerResponse.fromJson(json.decode(string));
          final savedToSharedPrefs = await saveToSharedPref(string);
          if (savedToSharedPrefs) {
            yield SplashEnded(serverResponse);
          }
        } else {
          print("Error");
        }
      }
    }
  }

  Future<String> getServerResponse() async {
    var dio = Dio();
    try {
      Response response = await dio.get(AppConstants.GET_RESPONSE_API,
          options: Options(connectTimeout: 10000, receiveTimeout: 10000));
      String string = json.encode(response.data);
      return string;

    } on DioError catch (e) {
      return null;
      }
    }

  }

  Future<bool> saveToSharedPref(String string) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(AppConstants.SERVER_RESPONSE_KEY, string);
    return true;
  }

  Future<String> readFromSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString(AppConstants.SERVER_RESPONSE_KEY) != null) {
      String string = prefs.getString(AppConstants.SERVER_RESPONSE_KEY);
      return string;
    } else {
      return null;
  }

  /* Future<bool> _startTimer(int secondsToLoad) {
    return Future.delayed(
        Duration(seconds: secondsToLoad),
        () {
          return true;
        });
  } */
}
