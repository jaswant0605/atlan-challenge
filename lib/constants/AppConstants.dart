class AppConstants {
  static const String SERVER_RESPONSE_KEY = "serverResponse";
  static const String GET_RESPONSE_API = "https://firebasestorage.googleapis.com/v0/b/collect-plus-6ccd0.appspot.com/o/mobile_tasks%2Fform_task.json?alt=media&token=d048a623-415e-41dd-ad28-8f77e6c546be";
  static const String SHORT_TEXT = "short_text";
  static const String DROPDOWN = "dropdown";
  static const String NUMBER = "number";
  static const String EMAIL = "email";
  static const String PHONE_NO = "phone_number";
  static const String RATING = "rating";
  static const String DATE = "date";
  static const String YES_NO = "yes_no";
  static const String FEEDBACKS_DATABASE_KEY = "feedbacks";

}