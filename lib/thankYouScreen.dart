import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'constants/AppConstants.dart';
import 'feedbackFormsScreen.dart';
import 'models/FeedbackResponse.dart';
import 'models/ServerResponse.dart';

class ThankYouScreen extends StatefulWidget {
  final ServerResponse serverResponse;

  ThankYouScreen(this.serverResponse);

  @override
  _ThankYouScreenState createState() => _ThankYouScreenState();
}

class _ThankYouScreenState extends State<ThankYouScreen> {
  @override
  void initState() {
    _getDatabase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          "THANK YOU",
          style: TextStyle(
              color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 14),
            child: widget.serverResponse.thankYouScreens[0] != null
                ? Text(
                    widget.serverResponse.thankYouScreens[0].title
                        .toUpperCase(),
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w800,
                        color: Colors.black),
                    textAlign: TextAlign.center,
                  )
                : Container(),
          ),
          SizedBox(height: 40),
          widget.serverResponse.thankYouScreens[0].properties.showButton
              ? RawMaterialButton(
                  elevation: 5,
                  shape: CircleBorder(),
                  splashColor: Colors.grey,
                  onPressed: () {
                    if (widget.serverResponse.thankYouScreens[0].properties
                            .buttonMode ==
                        "reload") {
                      Navigator.of(context).pushReplacement(CupertinoPageRoute(
                          builder: (context) =>
                              FeedbackForm(widget.serverResponse)));
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  fillColor: Colors.black,
                  child: Container(
                      height: 100.0,
                      width: 100.0,
                      child: Center(
                        child: Text(
                          "◀ " + widget.serverResponse.thankYouScreens[0].properties
                              .buttonText
                              .toUpperCase(),
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0),
                        ),
                      )),
                )
              : Container()
        ],
      ),
    );
  }

  _getDatabase() async {
    final Future<Database> database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'feedback_database.db'),
    );

    Future<List<FeedbackResponse>> dogs() async {
      // Get a reference to the database.
      final Database db = await database;

      // Query the table for all The Dogs.
      final List<Map<String, dynamic>> maps =
          await db.query(AppConstants.FEEDBACKS_DATABASE_KEY);

      print(maps);

      // Convert the List<Map<String, dynamic> into a List<Dog>.
      return List.generate(maps.length, (i) {
        return FeedbackResponse(
          id: maps[i]['id'],
          value: maps[i]['value'],
          field: maps[i]['field'],
          timeStamp: maps[i]['timeStamp'],
        );
      });
    }

    List<FeedbackResponse> feedbackResponses = await dogs();
    for (int i = 0; i < feedbackResponses.length; i++) {
      print(feedbackResponses[i].value);
    }
  }
}
