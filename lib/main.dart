import 'package:atlan_challenge/welcomeScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/splash_bloc.dart';
import 'bloc/splash_event.dart';
import 'bloc/splash_state.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Atlan Challenge',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final splashBloc = SplashBloc();

  @override
  void initState() {
    splashBloc.dispatch(HandleNavigation());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "ATLAN CLALLENGE",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 28.0,
                  fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
            ),
          ],
        )),
        BlocProvider(
          builder: (BuildContext context) => splashBloc,
          child: BlocListener(
            bloc: splashBloc,
            listener: (BuildContext context, SplashState state) {
              if (state is SplashEnded) {
                Navigator.of(context).pushReplacement(CupertinoPageRoute(builder: (context) => WelcomeScreen(state.serverResponse)));
              }
            },
            child: BlocBuilder(
              bloc: splashBloc,
              builder: (BuildContext context, SplashState state) {
                if (state is InitialSplashState) {
                  return Container();
                } else {
                  return _showLoading();
                }
              },
            ),
          ),
        ),
      ],
    ));
  }

  Widget _showLoading() {
    return Container(
        padding: EdgeInsets.only(bottom: 40.0),
        alignment: Alignment.bottomCenter,
        child: SizedBox(
          height: 50,
          width: 50,
          child: CircularProgressIndicator(

            valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
            strokeWidth: 7,
          ),
        )
        );
  }

  @override
  void dispose() {
    super.dispose();
    splashBloc.dispose();
  }
}
