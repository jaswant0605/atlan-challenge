import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'feedbackFormsScreen.dart';
import 'models/ServerResponse.dart';

class WelcomeScreen extends StatefulWidget {
  final ServerResponse serverResponse;

  WelcomeScreen(this.serverResponse);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          "WELCOME",
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 24, letterSpacing: 2),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Stack(
          children: <Widget>[
            Column(children: <Widget>[
            Image.asset("images/img_remote.png", height: 100, width: 100,),
              widget.serverResponse.welcomeScreens[0] != null
                  ? Text(
                widget.serverResponse.welcomeScreens[0].title.toUpperCase(),
                style:
                TextStyle(fontSize: 18.0, fontWeight: FontWeight.w800),
                textAlign: TextAlign.center,
              )
                  : Container(),
              SizedBox(height: 20),
          ],),

            widget.serverResponse.welcomeScreens[0].properties.showButton
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                            widget.serverResponse.welcomeScreens[0].properties
                                .description,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.blue,
                                fontStyle: FontStyle.italic),
                            textAlign: TextAlign.center),
                        SizedBox(height: 20),
                        RawMaterialButton(
                          elevation: 5,
                          shape: CircleBorder(),
                          splashColor: Colors.grey,
                          fillColor: Colors.black,
                          child: Container(
                            height: 100.0,
                            width: 100.0,
                            child: Center(child:
                            Text(
                              widget.serverResponse.welcomeScreens[0].properties
                                  .buttonText.toUpperCase() + " ▶",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                          ),
                          onPressed: _onButtonClicked,
                        )
                      ],
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  _onButtonClicked() {
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (context) => FeedbackForm(widget.serverResponse)));
  }
}
